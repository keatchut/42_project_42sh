
int		isbin(char *s)
{
	int i;

	i = 0;
	if (!s)
		return (0);
	while (s[i])
	{
		if (s[i] < 0 || s[i] > 126)
			return (1);
		i++;
	}
	return (0);
}
