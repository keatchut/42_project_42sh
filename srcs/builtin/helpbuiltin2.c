#include "mysh.h"

void	pathstat(int *c, char **mydir, char *av, struct stat *s)
{
	char	*cdp;

	cdp = NULL;
	if (!(*c = lstat(av, s)))
		giverealname(mydir, av);
	else if (retenv((*g_global->var)->lenv, "CDPATH"))
	{
		(cdp = ft_strdup(retenv((*g_global->var)->lenv, "CDPATH"))) ? 0
			: malloc_failed("pathstat");
		if ((cdp = cdpath(cdp, av))
		&& !(*c = lstat(cdp, s)))
		{
			giverealname(mydir, cdp);
			ft_strdel(&cdp);
		}
	}
}

void	nopaths(int opt[2], t_env *tmp, t_var **var)
{
	t_env *env;

	env = NULL;
	if (!opt[0])
	{
		(env = (t_env *)malloc(sizeof(t_env))) ? 0
			: malloc_failed("actualize_env");
		env->next = NULL;
		ft_strcpy(ft_strcpy(env->elem, "OLDPWD=") + 7, g_global->spwd->old_pwd);
		tmp ? (tmp->next = env)
			: ((*var)->lenv = tmp);
	}
	if (!opt[1])
	{
		(env = (t_env *)malloc(sizeof(t_env))) ? 0
			: malloc_failed("actualize_env");
		env->next = NULL;
		ft_strcpy(ft_strcpy(env->elem, "PWD=") + 4, g_global->spwd->pwd);
		tmp ? (tmp->next = env)
			: ((*var)->lenv = tmp);
	}
}
