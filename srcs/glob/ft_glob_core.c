#include "mysh.h"

char			*glob_core_while(char *str, int *j, t_var *var, int c)
{
	*j = 0;
	if (schrnob(str, '[') && hook_parser(&str, j, var) && !(*j))
	{
		if (!c)
		{
			ft_putstr_fd("mysh: no matches found: ", 2);
			ft_putendl_fd(str, 2);
		}
		return (NULL);
	}
	else if (schrnob(str, '{') && braces_parser(&str, j) && !(*j))
		return (NULL);
	else if ((schrnob(str, '?') || schrnob(str, '*'))
	&& star_parser(&str, j) && !(*j))
	{
		if (!c)
		{
			ft_putstr_fd("mysh: no matches found: ", 2);
			ft_putendl_fd(str, 2);
		}
		return (NULL);
	}
	return (str);
}

char			*schrnob(char *s, char c)
{
	int i;

	i = 0;
	if (!s)
		return (NULL);
	while (s[i])
	{
		if (s[i] == c && (!i || !nbcks(s, i - 1))
		&& !quoted(s, i) && (c != '[' || s[i + 1] != ' '))
			return (s + i);
		i++;
	}
	if (s[i] == c && !nbcks(s, i))
		return (s + i);
	return (NULL);
}

bool			check_hook_file_exist(char *str)
{
	DIR			*odir;
	t_dirent	*rdir;

	if (!(odir = opendir(".")))
		return (false);
	while ((rdir = readdir(odir)))
	{
		if (!ft_strcmp(rdir->d_name, str))
		{
			closedir(odir);
			return (true);
		}
	}
	closedir(odir);
	return (false);
}
