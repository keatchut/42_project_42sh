#include "mysh.h"

int			ft_is_emptyline(t_list *l)
{
	if (!l->next->next && !l->prev)
		return (1);
	return (0);
}

void		ft_free_all_jobs(void)
{
	t_job *tmp;

	while (g_jobs && g_jobs->prev)
		g_jobs = g_jobs->prev;
	while (g_jobs)
	{
		tmp = g_jobs;
		g_jobs = g_jobs->next;
		ft_free_all_processes(&tmp);
		ft_strdel(&(tmp->cmd));
		free(tmp);
		tmp = NULL;
	}
}

int			ft_is_job_activ(void)
{
	while (g_jobs && g_jobs->prev)
		g_jobs = g_jobs->prev;
	if (g_jobs && !g_jobs->prev && !g_jobs->next)
	{
		ft_strdel(&(g_jobs->cmd));
		free(g_jobs);
		g_jobs = NULL;
		return (0);
	}
	return (1);
}
