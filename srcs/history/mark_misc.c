
#include "mysh.h"

int				ft_find_r(int n)
{
	int rang;

	rang = 0;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / 10;
		rang++;
	}
	return (rang);
}
